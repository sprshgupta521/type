/* eslint-disable prettier/prettier */
import { IsInt, IsString,IsBoolean } from 'class-validator';
// eslint-disable-next-line prettier/prettier

export class CreateCatDto {
  @IsString()
  readonly username: string;

  @IsString()
  readonly firstname: string;

  @IsString() 
  readonly lastname: string;
  
  @IsBoolean()
  readonly UserType: string;
}