/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';
import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { BelongsToMany, Table,Model, Column, ForeignKey,BelongsTo, HasMany, DataType, AutoIncrement, HasOne} from 'sequelize-typescript';
import { type } from 'node:os';



/*@Table 
export class User extends Model<User> {
  
  
@Column({
  primaryKey: true,
  type: DataType.UUID,
  defaultValue: DataType.UUIDV4,
  allowNull: false,
})
id: string;


@Column
firstName: string;

@Column
lastName: string;
  save: any;


@ForeignKey(()=>Cat)
catId: number;

@BelongsTo(()=>Cat)
Cat: Cat[];




}
// one to many

@Table 
export class Cat extends Model<Cat> {
  /**
   * The name of the Cat
   * @example Kitty
    **/

    

 /* @Column({
    type:DataType.STRING(50),
  })
  name: string;

  @Column({type:DataType.STRING(50),
  })
  num: string;

   @HasMany(()=> User)
   User: User[];
 



  @ApiProperty({ example: 1, description: 'The age of the Cat' })
  age: number;

  @ApiProperty({
    example: 'Maine Coon',
    description: 'The breed of the Cat',
  })
  breed: string;
}


@Table 
export class IsLive extends Model{
 

  @Column
  addressId: number
   @ForeignKey(()=>User)
   user: User[]

  
  
  @ForeignKey(()=>Cat)
  cat: Cat[]
  @Column
  catId: number
  
} */

export type catDocument = Cat & Document;

Schema()
export class Cat {
  @Prop()
  username: string;

  @Prop()
  firstname: string;

  @Prop()
   lastname: string;
 @Prop()
 UserType : string;
}; 


export const CatSchema = SchemaFactory.createForClass(Cat);