
import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';
import { Cat, catDocument } from './entities/cats.schema';
import { InjectModel } from '@nestjs/sequelize';
import {Model} from 'mongoose';



@Injectable()
export class CatService {
  
  constructor(
    @InjectModel(Cat) private readonly catModel: Model<catDocument>,
   ) {}

 async create(CreateCatDto: CreateCatDto): Promise<Cat> {
    const user = new this.catModel(CreateCatDto);
    
    return user.save();
  }

  async findAll(): Promise<Cat[]> {
    return this.catModel.find().exec();
  }

 
  async findOne(id: string): Promise<Cat> {
    
    return this.catModel.findOne({
      where: {
        id,
      },
    });
  }

}
  
