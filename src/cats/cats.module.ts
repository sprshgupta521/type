/* eslint-disable prettier/prettier */
import { Module,forwardRef } from '@nestjs/common';
import { CatsController } from './cats.controller';
import { CatService } from './cats.services';
import { SequelizeModule } from '@nestjs/sequelize';
import {CatSchema,Cat} from './entities/cats.schema';
import {MongooseModule} from '@nestjs/mongoose';


@Module({
  imports: [ MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema}])],
  controllers: [CatsController],
  providers: [CatService],
})
export class CatsModule {}