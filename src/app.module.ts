import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import {SequelizeModule} from '@nestjs/sequelize';
import { MongooseModule} from '@nestjs/mongoose';

@Module({
  imports: [ MongooseModule.forRoot('mongodb://localhost:27017/test'),
,CatsModule],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
