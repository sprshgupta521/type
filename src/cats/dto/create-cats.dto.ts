import { IsInt, IsString } from 'class-validator';
import { StringifyOptions } from 'node:querystring';
// eslint-disable-next-line prettier/prettier

export class CreateCatsDto {
  @IsString()
  readonly name: string;

  @IsString() 
  readonly num: string;

}