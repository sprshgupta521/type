/* eslint-disable prettier/prettier */
import * as Joi from 'joi';

export const FOO_SCHEMA = Joi.object({
  username: Joi.string().required(),
  firstname: Joi.string().required(),
  lastname: Joi.string().required(),
  Usertype: Joi.boolean(),
});


