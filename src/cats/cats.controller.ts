/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
import { Body, Controller, Get, Param, Post,Delete ,Res,HttpStatus,UsePipes} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {  CatService } from './cats.services';
import { CreateCatDto } from './dto/create-cat.dto';
import {CreateCatsDto} from './dto/create-cats.dto';
import { JoiValidationPipe } from '../joi-valiadtion.pipe';
import { Cat} from './entities/cats.schema';
import {FOO_SCHEMA} from '../validation.schema';


@ApiBearerAuth()
@ApiTags('Cat')
@Controller('cats')
export class CatsController {
  constructor(private readonly CatService: CatService) {}

  @Post()
  @UsePipes(new JoiValidationPipe(FOO_SCHEMA))
  @ApiOperation({ summary: 'Create cat' })
  async create(@Body() createCatDto: CreateCatDto): Promise<Cat> {
    return this.CatService.create(createCatDto);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'The found record',
  })
  findAll(): Promise<Cat[]>{
    return this.CatService.findAll();
  }

}